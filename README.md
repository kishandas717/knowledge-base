# KnowledgebaseBackend

This project was developed with Node Js, Express Js, Sequelize ORM, and MySQL.

## Install NPM Packages

Run `npm install` in the project root directory.

## Create .env configuration file with the below variables

PROTOCOL= 'your protocol'
PORT= 'your port'
SECRET= 'your secret'
HOST= 'your host'
USER= 'Databade username'
PASSWORD= 'Database Password'
DB= 'Database Name'
DIALECT= 'Database' for example: 'mysql'

Add start MySQL Database

## Development server

Run `npm start` or `nodemon` for a dev server. The app will automatically reload if you change any of the source files.


