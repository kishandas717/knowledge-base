module.exports = (sequelize, Sequelize) => {
    const Content = sequelize.define("contents", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: Sequelize.STRING
        },
        author: {
            type: Sequelize.STRING
        },
        file_name: {
            type: Sequelize.STRING
        },
        file_url: {
            type: Sequelize.STRING
        }
    },
        {
            timestamps: false,
        }
    );

    return Content;
};