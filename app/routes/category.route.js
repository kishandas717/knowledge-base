// Importing NPM Packages
const express = require("express");
const router = express.Router();
const category = require('../controllers/category.controller');
const middleware = require('../middleware/jwt-middleware');

// Create Category
router.post('/', middleware.authorize, category.create);

// Retrive All Categories
router.get('/', middleware.authorize, category.findAll);

// Retrive Single Category
router.get('/:id', middleware.authorize, category.findOne);

// Update Category
router.put('/:id', middleware.authorize, category.update);

// Delete Category
router.delete('/:id', middleware.authorize, category.delete);

module.exports = router;