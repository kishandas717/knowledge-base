// Importing NPM Packages
const express = require("express");
const router = express.Router();
const content = require('../controllers/content.controller');
const fileUpload = require('../controllers/upload-file.controller');
const middleware = require('../middleware/jwt-middleware');

// Create Category
router.post('/', middleware.authorize, content.create);

// Retrive All Contents
router.get('/', middleware.authorize, content.findAll);

// Retrive Single Content
router.get('/:id', middleware.authorize, content.findOne);

// Update Content
router.put('/:id', middleware.authorize, content.update);

// Delete Content
router.delete('/:id', middleware.authorize, content.delete);

// Uplaod File
router.post('/upload', middleware.authorize, fileUpload.uploadFile);

module.exports = router;