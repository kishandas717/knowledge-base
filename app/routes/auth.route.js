// Importing NPM Packages
const express = require("express");
const router = express.Router();
const auth = require('../controllers/auth.controller');
const { check } = require("express-validator");

// User SignUp
router.post('/signup', [
    check("username", "Please Enter a Valid Username").not().isEmpty(),
    check("email", "Please enter a valid email").isEmail(),
    check("password", "Please enter a valid password").isLength({min: 6})], auth.signup);

// User SignIn
router.post('/signin', [
    check("email", "Please enter a valid email").isEmail(),
    check("password", "Please enter a valid password").isLength({
      min: 6
    })
  ], auth.sigin);

module.exports = router;