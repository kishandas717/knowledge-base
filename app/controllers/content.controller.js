const db = require('../../dbconnect');
const { content } = require('../../dbconnect');
const Content = db.content;
const Op = db.Sequelize.Op;

// Create and Save a new Content
exports.create = (req, res) => {
    // Validating request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const { title, author, file_name, file_url, categoryId } = req.body;

    // Create content
    const content = {
        title,
        author,
        file_url,
        file_name,
        categoryId
    }

    // Save Content in the database
    Content.create(content)
        .then(data => {
            res.status(201).send({
                success: true,
                status: 201,
                message: "Successfully created a content.",
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                status: 500,
                message: err.message || "Some error occurred while creating the content."
            });
        });
};

// Retrieve all Contents from the database.
exports.findAll = (req, res) => {
    Content.findAll()
        .then(data => {
            res.status(200).send(data);
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                status: 500,
                message: err.message || "Some error occurred while retrieving contents."
            });
        });
};

// Find a single Content with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Content.findByPk(id)
        .then(data => {
            res.status(200).send(data);
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                status: 500,
                message: "Error retrieving content with id:" + id
            });
        });
};

// Update a Content by the id
exports.update = (req, res) => {
    const id = req.params.id;

    const { title, author, file_name, file_url, categoryId } = req.body;
    const content = {
        title,
        author,
        file_name,
        file_url,
        categoryId
    }

    Content.update(content, { where: { id: id } })
        .then(data => {
            if (data == 1 || data == 0) {
                res.status(200).send({
                    success: true,
                    status: 200,
                    message: "Content updated successfully."
                });
            } else {
                res.status(404).send({
                    success: false,
                    status: 404,
                    message: "Cannot update content with id:" + id
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                status: 500,
                message: "Error updating content with id:" + id
            });
        });
};

// Delete a Content with the specified id
exports.delete = (req, res) => {
    const id = req.params.id;

    Content.destroy({ where: { id: id } })
        .then(data => {
            if (data == 1) {
                res.status(200).send({
                    success: true,
                    status: 200,
                    message: "Content deleted successfully!"
                });
            } else {
                res.send({
                    success: false,
                    status: 404,
                    message: "Cannot delete content with id:" + id
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                status: 500,
                message: "Could not delete content with id=" + id
            });
        });
};