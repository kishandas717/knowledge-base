// Importing Required NPM Packages
require('dotenv').config();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");

// Importing the User Model
const User = require('../../dbconnect').user;

// SignUp
exports.signup = async (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const { username, email, password } = req.body;
    try {
        let user = await User.findOne({ where: { email: email } });
        if (user) {
            return res.status(400).json({
                status: 400,
                message: "User already exists with this email. Please try with another email."
            });
        }

        user = {
            username,
            email,
            password
        };

        const salt = await bcrypt.genSalt(10); // Generated Salt
        user.password = await bcrypt.hash(password, salt); // Hashing Password

        // Creating User
        await User.create(user)
            .then(data => {
                jwt.sign({ id: data.datavalues }, process.env.SECRET, { expiresIn: 86400 }, (err, token) => {
                    if (err) throw err;
                    res.status(201).send({
                        success: true,
                        status: 201,
                        message: "Registered Successfully.",
                        token: token
                    });
                });
            })
            .catch(err => {
                console.log(err)
                res.status(500).send({
                    success: false,
                    status: 500,
                    message: "Error occurred while registering User.",
                    token: null
                })
            })
    } catch (err) {
        console.log(err)
        res.status(500).send({
            success: false,
            status: 500,
            message: "Error occurred while registering User.",
            token: null
        })
    }
}

// Sign In
exports.sigin = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const { email, password } = req.body;
    try {
        let user = await User.findOne({ where: { email: email } }); // Checking weather User exits or not
        if (!user)
            return res.status(400).json({
                success: false,
                status: 400,
                message: "User with this email not exist.",
                token: null
            });

        const isMatch = await bcrypt.compare(password, user.password); // Matching Password
        if (!isMatch)
            return res.status(400).json({
                success: false,
                status: 400,
                message: "Incorrect Password.",
                token: null
            });

        jwt.sign({ id: user.datavalues }, process.env.SECRET, { expiresIn: 86400 }, (err, token) => {
            if (err) throw err;
            res.status(200).json({
                success: true,
                status: 200,
                message: "Logged in successfully.",
                data: user,
                token: token,
            });
        }
        );
    } catch (error) {
        res.status(500).json({
            success: false,
            status: 500,
            message: error.message || "Server Error Occurred.",
            data: null,
            token: null
        });
    }
}