const db = require('../../dbconnect');
const Category = db.category;
const Op = db.Sequelize.Op;

// Create and Save a new Category
exports.create = (req, res) => {
    // Validating request
    if (!req.body) {
        res.status(400).send({
            message: "Category content can not be empty!"
        });
        return;
    }

    const { name, description } = req.body;

    // Create category
    const category = {
        name,
        description
    }

    // Save Category in the database
    Category.create(category)
        .then(data => {
            res.status(201).send({
                success: true,
                status: 201,
                message: "Successfully created a category.",
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                status: 500,
                message: err.message || "Some error occurred while creating the category."
            });
        });
};

// Retrieve all Categories from the database.
exports.findAll = (req, res) => {
    Category.findAll()
        .then(data => {
            res.status(200).send(data);
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                status: 500,
                message: err.message || "Some error occurred while retrieving categories."
            });
        });
};

// Find a single Category with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Category.findByPk(id)
        .then(data => {
            res.status(200).send({
                success: true,
                status: 200,
                message: "Retrived category with id: " + id + " successfully.",
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                status: 500,
                message: "Error retrieving category with id:" + id
            });
        });
};

// Update a Category by the id
exports.update = (req, res) => {
    const id = req.params.id;

    const { name, description } = req.body;
    const category = {
        name,
        description
    }

    Category.update(category, { where: { id: id } })
        .then(data => {
            if (data == 1 || data == 0) {
                res.status(200).send({
                    success: true,
                    status: 200,
                    message: "Category updated successfully."
                });
            } else {
                res.status(404).send({
                    success: false,
                    status: 404,
                    message: "Cannot update category with id:" + id
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                status: 500,
                message: "Error updating category with id:" + id
            });
        });
};

// Delete a Category with the specified id
exports.delete = (req, res) => {
    const id = req.params.id;

    Category.destroy({ where: { id: id } })
        .then(data => {
            if (data == 1) {
                res.status(200).send({
                    success: true,
                    status: 200,
                    message: "Category deleted successfully!"
                });
            } else {
                res.send({
                    success: false,
                    status: 404,
                    message: "Cannot delete category with id:" + id
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                status: 500,
                message: "Could not delete category with id=" + id
            });
        });
};