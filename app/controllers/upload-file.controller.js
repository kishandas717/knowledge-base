// Importing NPm Packages
require('dotenv').config();
const multer = require('multer');
const multers3 = require('multer-s3');
const aws = require('aws-sdk');

// Config AWS S#
const s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
})

// Multer AWS S3 Storage
const storageAWS = multers3({
    s3: s3,
    bucket: 'knowledgebase-app',
    acl: 'public-read',
    key: function (req, file, cb) {
        let datetimestamp = Date.now();
        let fileName = file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1];
        cb(null, 'uploads/photos/'+ fileName);
    }

})

//Multer disk storage settings
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads');
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === "application/pdf" || file.mimetype === "application/msword") {
        cb(null, true);
    } else {
        cb("File type cannot be uploaded", false);
    }
};

// Upload to AWS S3 
const uploadToS3 = multer({
    fileFilter: fileFilter,
    storage: storageAWS
}).single('file');

// Upload to Local Express Server
const upload = multer({
    fileFilter: fileFilter,
    storage: storage
}).single('file');

exports.uploadFile = (req, res) => {
    upload(req, res, async function (err) {
        if (err instanceof multer.MulterError) {
            // A Multer error occurred when uploading.]
            res.send({
                success: false,
                message: "A Multer error occurred when uploading.",
                file_location: null,
                error_code: 1,
                err_desc: err
            });
            return;
        } else if (err) {
            // An unknown error occurred when uploading.
            res.send({
                success: false,
                message: "An error occurred when uploading.",
                file_location: null,
                error_code: 1,
                err_desc: err
            });
            return;
        }
        const file_location = process.env.PROTOCOL + '://' + process.env.HOST + ':' + process.env.PORT + '/' + req.file.destination + '/' + req.file.filename;
        res.status(200).send({
            success: true,
            message: "File Uploaded Successfully.",
            file_name: req.file.filename,
            file_location: file_location,
            error_code: 0,
            err_desc: null
        });
    });
}


exports.uploadFileToAWSS3 = (req, res) => {
    uploadToS3(req, res, async function (err) {
        if (err instanceof multer.MulterError) {
            // A Multer error occurred when uploading.]
            res.send({
                success: false,
                message: "A Multer error occurred when uploading.",
                file_location: null,
                error_code: 1,
                err_desc: err
            });
            return;
        } else if (err) {
            // An unknown error occurred when uploading.
            res.send({
                success: false,
                message: "An error occurred when uploading.",
                file_location: null,
                error_code: 1,
                err_desc: err
            });
            return;
        }
        const file_location = process.env.PROTOCOL + '://' + process.env.HOST + ':' + process.env.PORT + '/' + req.file.destination + '/' + req.file.filename;
        res.status(200).send({
            success: true,
            message: "File Uploaded Successfully.",
            file_name: req.file.filename,
            file_location: file_location,
            error_code: 0,
            err_desc: null
        });
    });
}