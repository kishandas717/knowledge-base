// Importing NPM Packages
require('dotenv').config();
const jwt = require("jsonwebtoken");

exports.authorize = (req, res, next) => {
    const token = req.header("token");
    if (!token) return res.status(401).json({ status: 401, message: "Authorization Error." });

    try {
        const decoded = jwt.verify(token, process.env.SECRET);
        req.user = decoded.user;
        next();
    } catch (e) {
        res.status(401).send({ 
            success: false,
            status: 401,
            message: e.message || "Invalid Token."
         });
    }
}