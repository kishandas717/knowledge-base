require('dotenv').config();
const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    process.env.DB,
    process.env.USER,
    process.env.PASSWORD,
    {
        host: process.env.HOST,
        dialect: process.env.DIALECT,
        operatorsAliases: 0,
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./app/models/user.model")(sequelize, Sequelize);
db.category = require("./app/models/category.model")(sequelize, Sequelize);
db.content = require("./app/models/content.model")(sequelize, Sequelize);

db.category.hasMany(db.content, { as: "contents" });
db.content.belongsTo(db.category, {
    foreignKey: "categoryId",
    as: "category",
})

module.exports = db;
