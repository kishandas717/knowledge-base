// Importing NPM packages
require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const http = require('http');

// Defining PORT
const port = process.env.PORT || 3000;

// Routes
const auth = require('./app/routes/auth.route');
const category = require('./app/routes/category.route');
const content = require('./app/routes/content.route');

// App connection (Created Server)
const app = express();
const server = http.createServer(app);

app.use('/uploads', express.static(__dirname + '/uploads'));

// parse requests of content-type - application/json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use("/auth", auth);
app.use("/category", category);
app.use("/content", content);

// Created a simple route
app.get('/', (req, res) => {
    res.json({ "message": "Welcome to Node Js Knowledge Base App." });
});

// Connecting to the MYSQL Database
const db = require("./dbconnect");
db.sequelize.sync();

// listen for requests
server.listen(port, () => {
    console.log("Server is listening on port " + port);
});

